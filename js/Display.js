class Display {
    constructor(oldValueDisplay, currentValueDisplay) {
        this.currentValueDisplay = currentValueDisplay;
        this.oldValueDisplay = oldValueDisplay;
        this.calculator = new Calculator();
        this.operationType = undefined;
        this.currentValue = '';
        this.oldValue = '';
        this.sign = {
            plus: '+',
            divide: '/',
            times: 'x',
            minus: '-',
            power: '^',
        }
    }

    delete() {
        this.currentValue = this.currentValue.toString().slice(0, -1);
        this.printValues();
    }

    deleteAll() {
        this.currentValue = '';
        this.oldValue = '';
        this.operationType = undefined;
        this.printValues();
    }

    compute(type) {
        this.operationType !== 'equal' && this.calculate();
        this.operationType = type;
        this.oldValue = this.currentValue || this.oldValue;
        this.currentValue = '';
        this.printValues();
    }

    addNumber(number) {
        if (number === 'π' && number !== '.') {
            this.currentValue = this.calculator['pi']();

            this.printValues();
            return
        }
        if (number === '.' && this.currentValue.includes('.')) return
        this.currentValue = this.currentValue.toString() + number.toString();
        this.printValues();
    }

    printValues() {
        this.currentValueDisplay.textContent = this.currentValue;
        this.oldValueDisplay.textContent = `${this.oldValue} ${this.sign[this.operationType] || ''}`;
    }

    calculate() {
        const oldValue = parseFloat(this.oldValue);
        const currentValue = parseFloat(this.currentValue);

        if (isNaN(currentValue) || isNaN(oldValue)) return
        this.currentValue = this.calculator[this.operationType](oldValue, currentValue);
    }

    // Single Numbers Operators

    computeSingle(type) {
        this.operationType = type;
        this.singleNumberCalculate();
        // inmediate printing below
        this.currentValueDisplay.textContent = '';
        this.oldValueDisplay.textContent = this.currentValue;
        this.currentValue = '';
    }

    singleNumberCalculate() {
        const currentValue = parseFloat(this.currentValue);
        if (isNaN(currentValue)) return
        this.currentValue = this.calculator[this.operationType](currentValue);
    }

}
class Calculator {
    plus(num1, num2) {
        return num1 + num2;
    }
    minus(num1, num2) {
        return num1 - num2;
    }
    times(num1, num2) {
        return num1 * num2;
    }
    divide(num1, num2) {
        return num1 / num2;
    }
    percent(num1) {
        return num1 / 100;
    }
    squared(num1) {
        return Math.pow(num1, 2);
    }
    squareRoot(num1) {
        return Math.sqrt(num1);
    }
    rationalFunction(num1) {
        return 1 / num1;
    }
    factorial(num1) {
        let result = 1;
        for (let i = 1; i <= num1; i++) {
            result = result * i;
        }
        return result;
    }
    power(num1, num2) {
        return Math.pow(num1, num2);
    }
    cubeRoot(num1) {
        return Math.pow(num1, 0.333);
    }
    cubePower(num1) {
        return Math.pow(num1, 3);
    }
    pi() {
        return Math.PI.toFixed(5);
    }
    log(num1) {
        return Math.log(num1);
    }
    logN(num1) {
        return Math.LN10(num1);
    }
}
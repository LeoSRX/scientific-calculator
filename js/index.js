const oldValueDisplay = document.getElementById('oldValue');
const currentValueDisplay = document.getElementById('currentValue');
const numbersButtons = document.querySelectorAll('.number');
const operatorsButtons = document.querySelectorAll('.operator')
const singleOperators = document.querySelectorAll('.singleNumberOperator')

const display = new Display(oldValueDisplay, currentValueDisplay);

numbersButtons.forEach(button => {
    button.addEventListener('click', () => display.addNumber(button.innerHTML));
});

operatorsButtons.forEach(button => {
    button.addEventListener('click', () => display.compute(button.value))
})

singleOperators.forEach(button => [
    button.addEventListener('click', () => display.computeSingle(button.value))
])